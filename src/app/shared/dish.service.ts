import { Dish } from './dish.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable()
export class DishService {
  dishesChange = new Subject<Dish[]>();
  dishesFetching = new Subject<boolean>()

  constructor(private http: HttpClient) {
  }

  private dishes: Dish[] = [];


  fetchDishes() {
    this.dishesFetching.next(true);
    this.http.get<{ [id: string]: Dish }>('https://plovo-5e346-default-rtdb.firebaseio.com/dishes.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const dishData = result[id];
          return new Dish(
            id,
            dishData.name,
            dishData.description,
            dishData.imageUrl,
            dishData.price,
          );
        });
      }))
      .subscribe(dishes => {
        this.dishes = dishes;
        this.dishesChange.next(this.dishes.slice());
        this.dishesFetching.next(false);
      }, error => {
        this.dishesFetching.next(false);
      });
  }


  getDishes() {
    return this.dishes.slice();
  }

  getDish(index: number) {
    return this.dishes[index];
  }

  addDish(dish: Dish) {
    this.dishes.push(dish);
    this.dishesChange.next(this.dishes);
  }
}
